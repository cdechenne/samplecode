﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AlgorithmLibrary;
using System.ComponentModel.Composition;

namespace SieveOfEratoAlgorithm
{
    [Export(typeof(AlgorithmLibrary.IAlgorithm))]
    public class SieveOfEratoAlgorithm : AlgorithmLibrary.IAlgorithm
    {
        public List<int> ComputeAlgorithmAsync(IProgress<AlgorithmProgress> reporter, CancellationToken cancelToken)
        {
            // TODO
            return new List<int>();
        }

        public string Description
        {
            get
            {
                return "Algorithm created by Eratosthenes.";
            }
        }

        public string Name
        {
            get
            {
                return "Sieve of Eratosthenes (TODO)";
            }
        }
    }
}
