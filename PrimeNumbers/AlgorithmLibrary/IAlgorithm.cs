﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AlgorithmLibrary
{
    public interface IAlgorithm
    {
        string Name { get; }
        string Description { get; }
        List<int> ComputeAlgorithmAsync(IProgress<AlgorithmProgress> reporter, CancellationToken cancelToken);
    }
}
