﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlgorithmLibrary
{
    public class AlgorithmProgress
    {
        public int ItemsProcessed { get; set; }
        public int LastResultFound { get; set; }
        public int TotalResultsFound { get; set; }
    }
}
