﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PrimeNumbers.UserControls
{
    /// <summary>
    /// Interaction logic for AlgorithmUserControl.xaml
    /// </summary>
    public partial class AlgorithmUserControl : UserControl
    {
        public AlgorithmUserControl()
        {
            InitializeComponent();
        }

        private void RunAlgorithm_Click(object sender, RoutedEventArgs e)
        {
            ((ViewModels.AlgorithmViewModel)this.DataContext).RunAlgorithmAsync();
        }
    }
}
