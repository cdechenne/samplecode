﻿using PrimeNumbers.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PrimeNumbers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [ImportMany]
        private IEnumerable<AlgorithmLibrary.IAlgorithm> _availableAlgorithms;

        public MainWindow()
        {
            AlgorithmViewModels = new ObservableCollection<AlgorithmViewModel>();

            InitializeComponent();

            // For now, just bind this to the datacontext
            this.DataContext = this;

            // Create a new catalog
            var catalog = new AggregateCatalog();

            // Fill it with assemblies from the same executing directory
            catalog.Catalogs.Add(new DirectoryCatalog(System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)));

            // Now create a composition container for the parts in the catalog
            var container = new CompositionContainer(catalog);

            // Compose all the parts for this object
            try
            {
                container.ComposeParts(this);
            }
            catch(CompositionException eli)
            {
                // Just post a message box for now
                MessageBox.Show("Could not load dlls." + eli.Message);
            }

            // Now that the algorithms are filled in, create view models out of them
            foreach (AlgorithmLibrary.IAlgorithm algorithm in _availableAlgorithms)
                AlgorithmViewModels.Add(new AlgorithmViewModel(algorithm));
        }

        public ObservableCollection<AlgorithmViewModel> AlgorithmViewModels { get; set; }
    }
}
