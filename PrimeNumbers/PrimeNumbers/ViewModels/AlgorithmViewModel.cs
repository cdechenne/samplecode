﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PrimeNumbers.ViewModels
{
    public class AlgorithmViewModel : BaseViewModel
    {
        private AlgorithmLibrary.IAlgorithm _algorithm;
        private int _itemsProcessed;
        private int _lastResultFound;
        private int _totalResultsFound;

        public AlgorithmViewModel(AlgorithmLibrary.IAlgorithm algorithm)
        {
            _algorithm = algorithm;
        }

        private void Algorithm_ProgressChanged(object sender, AlgorithmLibrary.AlgorithmProgress e)
        {
            // Update the view model
            ItemsProcessed = e.ItemsProcessed;
            LastResultFound = e.LastResultFound;
            TotalResultsFound = e.TotalResultsFound;
        }

        public async void RunAlgorithmAsync()
        {
            Progress<AlgorithmLibrary.AlgorithmProgress> progress = new Progress<AlgorithmLibrary.AlgorithmProgress>();
            System.Threading.CancellationToken cancellationToken = new System.Threading.CancellationToken();

            // Link up the progress reporting with a callback
            progress.ProgressChanged += Algorithm_ProgressChanged;

            // Launch the task asyncronously
            await Task.Run<List<int>>(() =>
            {
                return _algorithm.ComputeAlgorithmAsync(progress, cancellationToken);
            });
        }

        public string Name
        {
            get { return _algorithm.Name; }
        }

        public int ItemsProcessed
        {
            get { return _itemsProcessed; }
            set
            {
                if (value != _itemsProcessed)
                {
                    _itemsProcessed = value;
                    RaisePropertyChangedEvent("ItemsProcessed");
                }
            }
        }

        public int LastResultFound
        {
            get { return _lastResultFound; }
            set
            {
                if (value != _lastResultFound)
                {
                    _lastResultFound = value;
                    RaisePropertyChangedEvent("LastResultFound");
                }
            }
        }

        public int TotalResultsFound
        {
            get { return _totalResultsFound; }
            set
            {
                if (value != _totalResultsFound)
                {
                    _totalResultsFound = value;
                    RaisePropertyChangedEvent("TotalResultsFound");
                }
            }
        }
    }
}
