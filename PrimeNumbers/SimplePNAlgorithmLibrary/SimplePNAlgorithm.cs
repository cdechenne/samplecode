﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace SimplePNAlgorithm
{ 
    [Export(typeof(AlgorithmLibrary.IAlgorithm))]
    public class SimplePNAlgorithm : AlgorithmLibrary.IAlgorithm
    {
        public List<int> ComputeAlgorithmAsync(IProgress<AlgorithmLibrary.AlgorithmProgress> reporter, CancellationToken cancelToken)
        {
            bool isPrime = true;
            List<int> primeNumbers = new List<int>();

            // Seed the prime numbers
            primeNumbers.Add(2);
            primeNumbers.Add(3);

            for(int i = 4; i < 1000000; i++)
            {
                isPrime = true;
                foreach(int primeNumber in primeNumbers)
                {
                    // First check to see if we should cancel
                    if (cancelToken.IsCancellationRequested)
                    {
                        // Return what we have
                        return primeNumbers;
                    }

                    if (i % primeNumber == 0)
                    {
                        // Not a prime number, so break early
                        isPrime = false;
                        break;
                    }
                }
                if (isPrime)
                {
                    // Add this to our list
                    primeNumbers.Add(i);
                }

                // Report some progress every 100 numbers
                if (i % 100 == 0)
                {
                    reporter.Report(new AlgorithmLibrary.AlgorithmProgress {
                        ItemsProcessed = i,
                        LastResultFound = primeNumbers.Last(),
                        TotalResultsFound = primeNumbers.Count()
                    });
                }
            }

            long number = 423498757712934875;
            List<int> composedOf = new List<int>();
            foreach(int primeNumber in primeNumbers)
            {
                if (number % primeNumber == 0)
                {
                    composedOf.Add(primeNumber);
                }
            }

            return primeNumbers;
        }
        public string Name
        {
            get { return "Simple"; }
        }

        public string Description
        {
            get { return "Simple brute force type algorithm."; }
        }
    }
}
